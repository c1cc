#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include "defs.h"

using namespace std;

struct constantitem
{
    const char* name;
    int   val;
};
constantitem constantpool[] =
{
    {"LIT", lit},
    {"OPR", opr},
    {"LOD", lod},
    {"STO", sto},
    {"CAL", cal},
    {"INC", Int},
    {"JMP", jmp},
    {"JPC", jpc},
    {"LAR", lar},
    {"SAR", sar},
    {"JPE", jpe},
    {"INIT", init},
    {"RET", ret},
    {"NEG", neg},
    {"ADD", add},
    {"MINUSS", minuss},
    {"MULT", mult},
    {"DIVV", divv},
    {"MOD",  mod},
    {"ANDAND", andand},
    {"OROR", oror},
    {"EQ",  eq},
    {"NEQ", neq},
    {"LT",  lt},
    {"LTE", lte},
    {"GT",  gt},
    {"GTE", gte},
    {"READD", readd},
    {"WRITEE", writee},
    {"WRITES", writes},
    {"NOTNOT", notnot},
    {"TRUE",   TRUE},
    {"FALSE",  FALSE},
    {"INT",    _INT},
    {"FLOAT",  _FLOAT},
    {"BOOL",   _BOOL},
    {"OPC",    _OPC},
    {"VOID",   _VOID}};


template<typename to, typename from>to lexical_cast(from const &x) {
  std::stringstream os;
  to ret;

  os << x;
  os >> ret;

  return ret;  
}

float translate_double(string str)
{
    return lexical_cast<float, string>(str);
}

int translate_keyword_or_int(string str)
{
    if(str.length() > 0)
    {
        transform(str.begin(), str.end(),str.begin(), ::toupper);
        if(str[0]>='A' &&str[0]<='Z')
        {
            for(size_t i = 0; i < sizeof(constantpool)/sizeof(constantpool[0]);i++)
            {
                if(str==constantpool[i].name)
                    return constantpool[i].val;
            }
            return 0;
        }
        else
        {
            return lexical_cast<int, string>(str);
        }
    }
    return 0;
}

int main(int argc, char* argv[])
{
    if(argc != 2) {
        cerr << "use output filename as argument." <<endl;
        return -1;
    }
    
    ofstream fout(argv[1], ios::out | ios::binary);
   
    int mode = -1;
    string line;
    instruction instr;
    while(getline(cin, line, '\n'))
    {
	line.erase(line.find_last_not_of("\r")+1);
        if(line==".code")
        {
            if(mode!=-1)
            {
                cerr<<"invalid assembly language file."<<endl;
                return -1;
            }
            mode = 0;
        }
        else if(line==".str")
        {
            if(mode!=0)
            {
                cerr<<"invalid assembly language file."<<endl;
                return -1;
            }
            mode = 1;

            {
                unsigned char *p=(unsigned char *)&instr;
                for(unsigned int i=0;i<sizeof(instruction);i++)
                    p[i]=0xff;
            }
            fout.write((char*)&instr, sizeof(instruction));
        }
        else
        {
            if(mode == 0)
            {
                string f, l, v, t;
                istringstream iss(line);
                iss>>f>>l>>v>>t;

                instr.f = (enum fct)translate_keyword_or_int(f);
                instr.l = translate_keyword_or_int(l);

                if(v.find('.')!=v.npos)
                {
                    instr.v.d = translate_double(v);
                }
                else
                {
                    instr.v.i = translate_keyword_or_int(v);
                }
                
                instr.t = translate_keyword_or_int(t);

                
                fout.write((char*)&instr, sizeof(instruction));
            }
            else
            {
                fout.write(line.c_str(), line.length()+1);
            }
        }
    }
    if(mode!=1)
    {
        cerr<<"invalid assembly language file."<<endl;
        return -1;
    }
    return 0;
}
