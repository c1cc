#+windows
(when (probe-file "./bin/c1cc.exe")
  (delete-file "./bin/c1cc.exe"))

#+ecl
(progn
  (asdf:make-build
   :c1
   :type :program
   :epilogue-code '(funcall (intern "MAIN" (find-package "C1CC")))
   :move-here ".\\bin")
  #+windows
  (progn
    (rename-file #P"./bin/c1-mono.exe" #P"c1cc.exe")
    )
  (quit))

#+sbcl
(progn
  (asdf:load-system :c1)
  (sb-ext:save-lisp-and-die "./bin/c1cc"
   :executable t :purify t
   :toplevel (lambda ()
               (eval (read-from-string "(c1cc::main)"))))
  (sb-ext:quit)
  )

#+ccl
(progn
  (asdf:load-system :c1)
  (ccl:save-application #+windows "./bin/c1cc.exe" #-windows "./bin/c1cc" :error-handler :quit
                        :prepend-kernel t
                        :toplevel-function (lambda ()
                                             (eval (read-from-string "(c1cc::main)"))))
  (ccl:quit)
  )



(error "Your implementation: ~a ~a is not supported at this time. Patches welcome!" (lisp-implementation-type) (lisp-implementation-version))

