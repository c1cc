CXX=g++
CXXFLAGS=-g -O2 -Wall -static #-Weffc++
BIN=bin
INCLUDE=include
INTERPRETER=interpreter
INTERPRETERD=interpreterd
ASSEMBLER=asm

LISP=ecl -load
# LISP=ccl --load
# LISP=sbcl --load

all:interpreter asm c1cc c1

interpreter: c1interpreter/interpreter.cpp
	$(CXX) $(CXXFLAGS) c1interpreter/interpreter.cpp -I $(INCLUDE) -o $(BIN)/$(INTERPRETER)
	$(CXX) $(CXXFLAGS) -DDEBUG c1interpreter/interpreter.cpp -I $(INCLUDE) -o $(BIN)/$(INTERPRETERD)


asm: c1asm/c1asm.cpp
	$(CXX) $(CXXFLAGS) c1asm/c1asm.cpp -I $(INCLUDE) -o $(BIN)/$(ASSEMBLER)

c1cc: makefile.lisp c1cc/c1.lisp c1cc/eir.lisp c1cc/c1cc.lisp
	cd $(CURDIR); $(LISP) makefile.lisp

c1: c1/c1.bat
	cp c1/c1.bat $(BIN)/c1.bat
	cp c1/c1.sh $(BIN)/c1.sh

clean:
	rm -rf $(BIN)/$(INTERPRETER)
