(defpackage #:eir
  (:use #:cl))

(in-package #:eir)

;; instruction = (F=OPCODE L=LEVEL V=VALUE T=TYPE)
;; stack-cell  = (V=VALUE  T=TYPE)
;; eir-code = (INSTRUCTIONS STRINGS)

(defvar *eir-opr-list*
  '(:lit :opr :lod :sto :cal :int :jmp :jpc :lar :sar :jpe :init))

(defvar *eir-opr-code-list*
  '(:ret :neg :add :minuss :mult :divv :mod :andand :oror :eq :neq
    :lt  :lte :gt  :gte :readd :writee :writes :notnot))

(defvar *eir-type-list* '(:int :float :bool :opc :void))

(defvar *eir-stack-size* 5000)

(defun eir-coerce (val type)
  val)

(defun eir-error (&rest args)
  (error "error: ~a" args)
  )

(defun eir-read (type)

  )

(defun eir-write (value)
  
  )

(defun eir-not (value)
  )

(defun eir-arithmetic-promote (&rest types)
  (cond
    ((member :float types) :float)
    (t :int)))

(defun interpret (code)
  (destructuring-bind (instructions strings) code
    (let ((.t 0)
          (.b 1)
          (.p 0)
          (.s (make-array *eir-stack-size*)))
      (labels ((.write (i v1 t1) (setf (elt .s i) (list v1 t1)))
               (.write2 (i l)  (setf (elt .s i) (list (first l) (second l))))
               (.value (i)     (first (elt .s i)))
               (.type  (i)     (second (elt .s i)))
               (.base (b l) (cond
                              ((= l -1) 0)
                              (t (loop while (> l 0)
                                    do
                                      (setf b (first (aref .s b)))
                                      (decf l)))))
               (.opr (o v1 t1) (ecase o
                               (:neg (list (ecase t1
                                             ((:int :float) (- v1))
                                             ((:bool) (eir-not v1))) t1))
                               (:not (list (eir-not v1) :bool))))

               (.opr2 (o v1 t1 v2 t2)
                 (ecase o
                   ((:add :sub :mult :div :mod)
                    (let ((rt (eir-arithmetic-promote t1 t2)))
                      (list
                       (eir-coerce
                        (funcall
                         (second (assoc o '((:add +)
                                            (:sub -)
                                            (:mult *)
                                            (:div  /)
                                            (:mod  eir-mod))))
                         (eir-coerce v1 rt) (eir-coerce v2 rt)) rt)
                       rt)))
                   ((:eq :neq :lt :le :gt :ge)
                    (let ((rt (eir-arithmetic-promote t1 t2)))
                      (list
                       (eir-coerce
                        (funcall
                         (second (assoc o '((:eq  =)
                                            (:neq /=)
                                            (:lt  <)
                                            (:le  <=)
                                            (:gt  >)
                                            (:ge  >=))))
                         (eir-coerce v1 rt) (eir-coerce v2 rt)) :bool)
                       :bool)))
                   ((:and :or)
                    (let ((rt (eir-arithmetic-promote t1 t2)))
                      (list
                       (eir-coerce
                        (funcall
                         (second (assoc o '((:and *)
                                            (:or  +))))
                         (eir-coerce v1 rt) (eir-coerce v2 rt)) :bool)
                       :bool))))))
        (.write 0 0 :int)
        (.write 1 0 :int)
        (.write 2 0 :int)
        (.write 3 0 :int)

        (loop do
             (format t "DBG: .P=~a ~%" .p)
             (destructuring-bind (.if .il .iv .it) (elt instructions .p)
               (incf .p)
               (ecase .if
                 (:lit (incf .t)
                       (.write .t .iv .it))
                 (:opr (ecase .iv
                         (:ret (setf .t (- .b 1))
                               (setf .p (.value (+ .t 3)))
                               (setf .b (.value (+ .t 2)))
                               ;; mov return-value to overwrite funcargs
                               (.write (+ .t 1 (- .il))
                                       (.value (+ .t 4)) (.type (+ .t 4)))
                               (setf   .t  (+ .t 1 (- .il))))
                         (:neg
                          (.write2 .t (.opr :neg  (.value .t) (.type .t))))
                         (:add
                          (decf .t)
                          (.write2 .t (.opr2 :add
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:minuss
                          (decf .t)
                          (.write2 .t (.opr2 :sub
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:mult
                          (decf .t)
                          (.write2 .t (.opr2 :mul
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:divv
                          (decf .t)
                          (.write2 .t (.opr2 :div
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:mod
                          (decf .t)
                          (.write2 .t (.opr2 :mod
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:andand
                          (decf .t)
                          (.write2 .t (.opr2 :and
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:oror
                          (decf .t)
                          (.write2 .t (.opr2 :or
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:eq
                          (decf .t)
                          (.write2 .t (.opr2 :eq
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:neq
                          (decf .t)
                          (.write2 .t (.opr2 :neq
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:lt
                          (decf .t)
                          (.write2 .t (.opr2 :lt
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:lte
                          (decf .t)
                          (.write2 .t (.opr2 :le
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:gt
                          (decf .t)
                          (.write2 .t (.opr2 :gt
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:gte
                          (decf .t)
                          (.write2 .t (.opr2 :ge
                                            (.value .t) (.type .t)
                                            (.value (1+ .t)) (.type (1+ .t)))))
                         (:readd
                          (incf .t)
                          (.write .t (eir-read .il) .il))
                         (:writee
                          (eir-write (.value .t))
                          (decf .t)
                          )
                         (:writes
                          (eir-write (elt strings (.value .t)))
                          (decf .t)
                          )
                         (:notnot
                          (.write2 .t (.opr :not (.value .t) (.type .t)))
                          )))
                 (:lod (incf .t)
                       (let ((i (+ (.base .b .il) .iv)))
                         (.write t (.value i) (.type i))))
                 (:sto (let ((i (+ (.base .b .il) .iv)))
                         (.write i (eir-coerce (.value t) (.type i))
                                 (.type i)))
                       (decf .t))
                 (:cal (.write (+ 1 .t) (.base .b .il) :int)
                       (.write (+ 2 .t) .b :int)
                       (.write (+ 3 .t) .p :int)
                       (setf .b (1+ .t))
                       (setf .p .iv))
                 (:int (incf .t .iv))
                 (:jmp (setf .p .iv))
                 (:jpc (when (= .iv 0)
                         (setf .p .iv))
                       (decf .t))
                 (:jpe (when (= .iv 1)
                         (setf .p .iv))
                       (decf .t))
                 (:init (let* ((i (+ (.base .b 0) .iv))
                               (v (.value i)))
                          (.write i (eir-coerce v .il) .il)))
                 (:lar  (let ((size (eir-coerce (.value .t) :int))
                              (loc  (eir-coerce (.value (1- .t)) :int)))
                          (when (or (< loc 0) (>= loc size))
                            (eir-error .p 'out-of-bound size loc))
                          (decf .t)
                          (let ((i (+ (.base .b .il) .iv loc)))
                            (.write .t (.value i) (.type i)))))
                 (:sar  (let ((size (eir-coerce (.value .t) :int))
                              (loc  (eir-coerce (.value (1- .t)) :int)))
                          (when (or (< loc 0) (>= loc size))
                            (eir-error .p 'out-of-bound size loc))
                          (let ((i (+ (.base .b .il) .iv loc)))
                            (.write i (eir-coerce (.value (- .t 2))(.type (- .t 2)))
                                    (.type (- .t 2))))
                          (decf .t 2)))))
           while (/= .p 0))))))

(defun format-code (code)
  (format t "Code:~%")
  (loop for addr from 0
     for instruction in (first code)
     do (format t "~2T~3D: ~A~%" addr instruction))
  (format t "Strings:~%")
  (loop for index from 0
     for string in (second code)
     do (format t "~2T~3D: ~A~%" index string)))

(defun write-asm (code)
  (format t ".code")
  (loop for addr from 0
     for instruction in (first code)
     do (format t "~%~{ ~A~}" instruction))
  (format t "~%.str")
  (loop for index from 0
     for string in (second code)
     do (format t "~%~A" string)))
