(cl:defpackage #:c1
  (:use #:cl #:cl-lex #:yacc #:alexandria #:cl-unification #:split-sequence))

(cl:defpackage #:c1-usercode
  (:use))

(cl:in-package #:c1)

(defun unescape-string (str)
  (labels ((read-integer (&optional (stream *standard-input*)
                                    (base 10) (max-char-count nil))
             (loop with result = 0
                for char-count from 0
                while (or (not max-char-count) (< char-count max-char-count))
                with ch = (peek-char nil stream nil :eof)
                if (eql ch :eof) do (loop-finish)
                else do (let ((digit (digit-char-p ch base)))
                          (if (not digit) (loop-finish))
                          (read-char stream nil nil)
                          (setf result (+ (* result base) digit)))
                finally (progn
                          (when (zerop char-count)
                            (cerror "A base ~a number expected but missing." base))
                          (return result)))))
    
    (with-output-to-string (os)
      (with-input-from-string (is str)
        (loop for ch = (read-char is nil :eof)
           until (eq ch :eof)
           if (eql ch #\\)
           do (let* ((tchar (read-char is nil :eof))
                     (rchar
                      (case tchar
                        (:eof (error "Escape symbol occured at end of string."))
                        (#\a #\bell)
                        (#\b #\backspace)
                        (#\f #\page)
                        (#\n #\linefeed)
                        (#\r #\return)
                        (#\t #\tab)
                        (#\v #\vt)
                        (#\' #\')
                        (#\" #\")
                        (#\\ #\\)
                        (#\? #\?)
                        ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8)
                         (unread-char tchar is)
                         (code-char (read-integer is 8 3)))
                        ((#\u #\x)
                         (code-char (read-integer is 16 4)))
                        (t (warn "~A is not a valid escape char." tchar)
                           tchar))))
                (write-char rchar os))
           else
           do (write-char ch os))))))


(define-string-lexer c1-lexer
  ("\\.[0-9]+"
   (return (values :float (with-input-from-string (s $@) (read s)))))
  ("[1-9][0-9]*\\.[0-9]*"
   (return (values :float (with-input-from-string (s $@) (read s)))))
  
  ("0[xX][0-9A-Fa-f]+"
   (return (values :integer (parse-integer (remove #\x $@ :test #'char-equal)
                                          :radix 16))))  
  ("0[0-7]+"
   (return (values :integer (parse-integer $@ :radix 8))))
  ("0"
   (return (values :integer (parse-integer $@))))
  ("[1-9][0-9]*"
   (return (values :integer (parse-integer $@))))  
  
  ("[\\\"]([^\\\"\\\\]*([\\\\].)*)*[\\\"]"
   (return (values :string #|(unescape-string |# (subseq $@ 1 (1- (length $@))) #|)|#)))
  ("[_A-Za-z][_A-Za-z0-9]*"
   (return (values :symbol (intern $@ '#:c1-usercode))))

  ("!=" (return (values :neq)))
  ("==" (return (values :eq)))
  ("<=" (return (values :leq)))
  (">=" (return (values :geq)))
  ("<"  (return (values :lss)))
  (">"  (return (values :gtr)))
  
  ("{" (return (values :lbrace)))
  ("}" (return (values :rbrace)))

  ("\\(" (return (values :lparen)))
  ("\\)" (return (values :rparen)))

  ("\\[" (return (values :lbracket)))
  ("\\]" (return (values :rbracket)))

  (";"   (return (values :semicolon)))
  (":"   (return (values :colon)))
  (","   (return (values :comma)))
  ("\\." (return (values :period)))
  ("~"   (return (values :tilde)))

  ("&&"  (return (values :and)))
  ("\\|\\|"  (return (values :or)))
         
  
  ("&"   (return (values :ampersand)))
  ("\\^" (return (values :caret)))
  ("!"   (return (values :exclamation)))
  ("->"  (return (values :arrow)))

  ("\\+" (return (values :plus)))
  ("-"   (return (values :minus)))
  ("\\*" (return (values :asterisk)))
  ("/"   (return (values :slash)))
  ("%"   (return (values :modulus)))
  ("="   (return (values :assign)))

  (#.(string #\space))
  (#.(string #\return))
  (#.(string #\linefeed))
  (#.(string #\tab))
  ("." (return (values :illegal-character (aref $@ 0))))
    
  )



(defun c1-lexer-test (testcase)
  (let ((lexer (c1-lexer testcase)))
    (loop
         (multiple-value-bind (type val) (funcall lexer)
           (unless type (return))
           (format t "~w~@[ ~w~]~%" type val)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun parser-list-item-selector (index)
    (lambda (&rest lst)
      (nth index lst)))

  (defun parser-list-items-selector (&rest index-list)
    (lambda (&rest lst)
      (mapcar (lambda (x) (nth x lst)) index-list)))

  (defun parser-prefix-adder (item)
    (lambda (&rest lst)
      (cons item lst)))
  
  (defun append1 (lst x)
    (append lst (list x)))
  
  (defun selective (&rest preds)
    (lambda (&rest args)
      (some (lambda (pred) (apply pred args)) preds))))

(define-parser c1-initializer-parser
  (:start-symbol initializer)
  (:terminals (:symbol :integer :float :string :lbrace :rbrace :comma))
  (initializer
   (:lbrace :rbrace (constantly nil))
   (:lbrace initializer-comma-list :rbrace (parser-list-item-selector 1)))
  (initializer-comma-list
   (object (rcurry #'cons nil))
   (object :comma initializer-comma-list (compose (curry 'apply 'cons) (parser-list-items-selector 0 2))))
  (object
   :symbol
   :integer
   :float
   :string
   initializer))
   
  
  

(define-parser c1-expr-parser
  (:start-symbol expr)
  (:terminals (:symbol :integer :float
                       :string :lparen :rparen :lbracket :rbracket :comma
                       :plus :minus :asterisk :slash :modulus :tilde :exclamation
                       :lss :leq :gtr :geq :eq :neq :assign :comma :and :or))

  (expr
   comma-expr
   )

  (nullable-expr-comma-list
   ()
   expr-comma-list)
  
  (expr-comma-list
   (assignment-expr)
   (assignment-expr :comma  expr-comma-list 
                    (compose (curry 'apply 'cons)
                             (parser-list-items-selector 0 2)))
   )

  (comma-expr
   assignment-expr
   (comma-expr :comma assignment-expr
               (compose (curry 'apply (parser-prefix-adder 'prog2))
                        ))
   )
  
  (assignment-expr
   boolean-expr2
   (equality-expr :assign assignment-expr
                  (compose (curry 'apply (parser-prefix-adder 'setf))
                           (parser-list-items-selector 0 2)))
   )

  (boolean-expr2
   boolean-expr1
   (boolean-expr2 :or boolean-expr1
                  (compose (curry 'apply (parser-prefix-adder 'or))
                           (parser-list-items-selector 0 2)))
                  
   )

  (boolean-expr1
   equality-expr
   (boolean-expr1 :and equality-expr
                  (compose (curry 'apply (parser-prefix-adder 'and))
                           (parser-list-items-selector 0 2)))
   )
                  
  
  (equality-expr
   comparison-expr
   (equality-expr :eq comparison-expr
                  (compose (curry 'apply (parser-prefix-adder '=))
                           (parser-list-items-selector 0 2)))
   (equality-expr :neq comparison-expr
                  (compose (curry 'apply (parser-prefix-adder '/=))
                           (parser-list-items-selector 0 2)))
   )
                  
  
  (comparison-expr
   addition-expr
   (comparison-expr :lss addition-expr
                  (compose (curry 'apply (parser-prefix-adder '<))
                           (parser-list-items-selector 0 2)))
   (comparison-expr :leq addition-expr
                  (compose (curry 'apply (parser-prefix-adder '<=))
                           (parser-list-items-selector 0 2)))
   (comparison-expr :gtr addition-expr
                  (compose (curry 'apply (parser-prefix-adder '>))
                           (parser-list-items-selector 0 2)))
   (comparison-expr :geq addition-expr
                  (compose (curry 'apply (parser-prefix-adder '>=))
                           (parser-list-items-selector 0 2)))
   )
                    

  (addition-expr
   production-expr
   (addition-expr :plus production-expr
                  (compose (curry 'apply (parser-prefix-adder '+))
                           (parser-list-items-selector 0 2)))
   (addition-expr :minus production-expr
                  (compose (curry 'apply (parser-prefix-adder '-))
                           (parser-list-items-selector 0 2)))
   )
   
   
                  
  
  (production-expr
   prefix-expr
   (production-expr :asterisk prefix-expr
                    (compose (curry 'apply (parser-prefix-adder '*))
                             (parser-list-items-selector 0 2)))
   (production-expr :slash    prefix-expr
                    (compose (curry 'apply (parser-prefix-adder '/))
                             (parser-list-items-selector 0 2)))

   (production-expr :modulus  prefix-expr
                    (compose (curry 'apply (parser-prefix-adder 'mod))
                             (parser-list-items-selector 0 2)))
   )

  
  (prefix-expr
   postfix-expr
   (:minus  prefix-expr
            (selective
             (lambda (a b) (declare (ignore a)) (when (numberp b) (- b)))
             (compose (curry 'apply (parser-prefix-adder '-))
                      (parser-list-items-selector 1))))
   (:lparen :symbol :rparen prefix-expr
            (compose (curry 'apply (parser-prefix-adder 'coerce))
                     (parser-list-items-selector 1 3)))

   (:tilde  prefix-expr
            (compose (curry 'apply (parser-prefix-adder 'bit-not))
                     (parser-list-items-selector 1)))
            
   (:exclamation prefix-expr
                 (compose (curry 'apply (parser-prefix-adder 'not))
                          (parser-list-items-selector 1)))

   (:asterisk    prefix-expr
                 (compose (curry 'apply (parser-prefix-adder 'cdr))
                          (parser-list-items-selector 1)))

   (:ampersand   prefix-expr
                 (compose (curry 'apply (parser-prefix-adder 'car))
                          (parser-list-items-selector 1)))
   )
   
  
  (postfix-expr
   atom-expr
   (postfix-expr :lbracket expr :rbracket
                 (compose (curry 'apply (parser-prefix-adder 'elt))
                          (parser-list-items-selector 0 2)))
   (postfix-expr :lparen nullable-expr-comma-list :rparen
                 (compose (curry 'apply (parser-prefix-adder 'apply))
                          (parser-list-items-selector 0 2)))
   (postfix-expr :dot  atom-expr
                 (compose (curry 'apply (parser-prefix-adder 'member))
                          (parser-list-items-selector 0 2)))
   (postfix-expr :arrow atom-expr
                 (compose (curry 'apply (parser-prefix-adder 'assoc))
                          (parser-list-items-selector 0 2)))
   )
     
  (atom-expr
   :symbol
   :integer
   :float
   :string
   (:lparen expr :rparen (compose #'second #'list))
   )

  )

(defun make-peekable-lexer (lexer)
  (let (sym val peeked)
    (lambda (&optional command)
      (ecase command
        ((nil :read)
         (if peeked
             (progn (setf peeked nil)
                    (values sym val))
             (progn (setf (values sym val) (funcall lexer))
                    (setf peeked nil)
                    (values sym val))))
        ((:peek)
         (if peeked
             (values sym val)
             (progn
               (setf (values sym val) (funcall lexer))
               (setf peeked t)
               (values sym val))))))))

(defun c1-peekable-lexer (string)
  (make-peekable-lexer (c1-lexer string)))


(defun c1-sentence (lexer)
  (loop with sym and val and type = :expr
     do (setf (values sym val) (funcall lexer :peek))
     if (member sym '(:lparen :lbracket :lbrace))
       collect (c1-paragraph lexer (getf '(:lparen   :paren
                                           :lbracket :bracket
                                           :lbrace   :brace) sym)) into sentence
       and if (eq sym :lbrace)
              do (setf type :form)
                 (loop-finish)
           end
     else if (member sym '(:rparen :rbracket :rbrace nil))
       do (loop-finish)
     else if (member sym '(:semicolon))
       do (funcall lexer)
          (setf type :statement)
          (loop-finish)
     else
       do (funcall lexer) and collect (or val sym) into sentence
     finally (return (cons type
                           sentence))))

(defun c1-paragraph (lexer &optional type)
  (let ((start-tag (getf '(:paren   :lparen
                           :bracket :lbracket
                           :brace   :lbrace) type))
        (end-tag (getf '(:paren   :rparen
                         :bracket :rbracket
                         :brace   :rbrace) type)))
    (when type
      (assert (eq (funcall lexer :read) start-tag)))
    (loop with sym #|and val|#
       do(setf (values sym #|val|#) (funcall lexer :peek))
       if (eq sym end-tag)
         do (funcall lexer)
            (loop-finish)
       else if (member sym '(:lparen :lbracket :lbrace))
         collect (c1-paragraph lexer (getf '(:lparen   :paren
                                             :lbracket :bracket
                                             :lbrace   :brace) sym)) into paragraph
       else if (member sym '(:rparen :rbracket :rbrace nil))
         do (error "Unmatched symbol ~a" (funcall lexer))
            (loop-finish)
       else
         collect (c1-sentence lexer) into paragraph
       finally
         (return (if type (cons type paragraph) paragraph)))))

(defvar *c1-builtin-type-keyword-list*
  '(c1-usercode::|void|
    c1-usercode::|int|
    c1-usercode::|bool|
    c1-usercode::|float|))


(defvar *c1-builtin-constant-list*
  '((c1-usercode::|true| c1-usercode::|bool| c1-usercode::|true|)
    (c1-usercode::|false| c1-usercode::|bool| c1-usercode::|false|)))


(defun flattenize (paragraph)
  (loop for item in paragraph
     if (consp item)
     append (case (car item)
              (:paren  `(:lparen   ,@(flattenize (cdr item)) :rparen))
              (:bracket `(:lbracket ,@(flattenize (cdr item)) :rbracket))
              (:brace  `(:lbrace   ,@(flattenize (cdr item)) :rbrace))
              (:expr   (flattenize (cdr item)))
              (:statement `(,@(flattenize (cdr item)) :semicolon))
              (:form   (flattenize (cdr item)))
              (t       (flattenize item)))
     else
     collect item
     end))



(defun make-flattenize-lexer (paragraph)
  (let ((p (flattenize paragraph)))
    (lambda ()
      (if (null p)
          nil
          (let ((v (prog1 (car p) (setf p (cdr p)))))
            (cond
              ((keywordp v) (values v nil))
              ((symbolp  v) (values :symbol  v))
              ((integerp v) (values :integer v))
              ((floatp   v) (values :float   v))
              ((stringp  v) (values :string  v))
              ((characterp v) (values :invalid-character v))
              (t            (error "unknown object occurred."))))))))
      
(defun c1-reparse-as-expr (sentence)
  (parse-with-lexer
   (make-flattenize-lexer sentence) c1-expr-parser))

(defun c1-reparse-as-initializer (sentence)
  (parse-with-lexer
   (make-flattenize-lexer sentence) c1-initializer-parser))


(defvar *c1-type-keyword-list* *c1-builtin-type-keyword-list*)
(defvar *c1-type-specifier-list* '(c1-usercode::|const|))

(defun c1-transform (paragraph)
  (labels ((recognize-type-keyword (seq &key (start 0) (end nil))
             (unless end
               (setf end (length seq)))

             (cond
               ((<= end start) (values nil 0))
               ((member (elt seq start) *c1-type-keyword-list*)
                (values (list (elt seq start)) 1))
               ((and (member (elt seq start) *c1-type-specifier-list*)
                     (< (1+ start) end)
                     (member (elt seq (1+ start)) *c1-type-keyword-list*))
                (values (list (elt seq start) (elt seq (1+ start))) 2))
               (t (values nil 0))))
           (analyze-array-dimension (dims declr)
             (mapcar (lambda (dim)
                       (assert (and (consp dim) (eq (car dim) :bracket)) ()
                               "invalid array declr form: ~a" declr)
                       (let ((val (and (cdr dim) (c1-reparse-as-expr (cdr dim)))))
                         (cond
                           ((integerp val) val)
                           ((null val) '*)
                           (t (error "invalid dim declaration ~a in declr form ~a"
                                     (cdr dim) declr))))) dims))
           (analyze-array-initializer (initializer-form)
             (c1-reparse-as-initializer initializer-form)
             )
           (transform-sentence (idx)
             (let ((cur-sentence (elt paragraph idx))
                   (declrtype)
                   (declrtype-length))
               (if (setf (values declrtype declrtype-length)
                         (recognize-type-keyword cur-sentence :start 1))
                   (ecase (first cur-sentence)
                     (:statement ;;declaration
                      (setf
                       (elt paragraph idx)
                       (cons
                        'declare
                        (loop for declr in (split-sequence
                                            :comma cur-sentence
                                            :start (1+ declrtype-length))
                           collecting
                             (match-case (declr)
                               ('(?varname) `(,?varname ,declrtype))
                               ('(?varname :assign . ?expr)
                                 `(,?varname ,declrtype
                                             ,(c1-reparse-as-expr ?expr)))
                               ('(?varname(:bracket (:expr . ?first-dim)) . ?rest)
                                 ;; array declaration form.
                                 `(,?varname (array
                                              ,declrtype
                                              ,(analyze-array-dimension
                                                (cdr declr) declr))))
                               ('(?funcname (:paren . ?arglist))
                                 (let ((arglist
                                        (when ?arglist
                                          (mapcar #'recognize-type-keyword
                                                  (split-sequence
                                                   :comma
                                                   (flattenize ?arglist))))))
                                   `(,?funcname (function ,declrtype ,arglist))
                                   ))
                               (t (error "invalid declr form: ~a" declr))
                               )))))
                     (:form
                      (setf
                       (elt paragraph idx)
                       (match-case ((subseq cur-sentence
                                            (1+ (length declrtype))))
                         (`(?funcname (:paren . ?arglist) (:brace . ?body))
                           (let ((funcname ?funcname)
                                 (arglist
                                  (when ?arglist
                                    (match-case (?arglist)
                                      ('((:expr . ?arglist-items))
                                        (mapcar
                                         (lambda (argitem)
                                           (let ((argtype
                                                  (recognize-type-keyword argitem)))
                                             (unless argtype
                                               (error "invalid funcarg type: ~a"
                                                      argitem))
                                             (if (eql (length argitem)
                                                      (length argtype))
                                                 `(,argtype)
                                                 `(,argtype
                                                   ,@(subseq argitem
                                                             (length argtype))))))
                                         (split-sequence
                                          :comma ?arglist-items)))
                                      (t
                                       (error "invalid funcarg declarations: ~a"
                                              ?arglist)
                                       ))))
                                 (body (c1-transform ?body)))
                             `(defun ,funcname ,arglist ,declrtype ,body)
                             ))
                         (`(?varname (:bracket (:expr . ?first-dim)) . ?rest)
                           (let* ((startpos (+ 1 declrtype-length 1))
                                  (dimcount (position
                                             t (subseq cur-sentence startpos)
                                             :key #'atom)))
                             (assert (and (eq (elt cur-sentence
                                                   (+ startpos dimcount))
                                              ':assign)
                                          (equal (nth (1+ idx) paragraph)
                                                 '(:statement))) ()
                                     "invalid array declr form: ~a" cur-sentence)
                             (setf (cdr (nthcdr idx paragraph))
                                            (nthcdr (1+ (1+ idx)) paragraph))
                             `(declare (,?varname
                                        (array ,declrtype
                                                ,(analyze-array-dimension
                                                  (subseq cur-sentence startpos
                                                          (+ startpos dimcount))
                                                  cur-sentence))
                                        ,(analyze-array-initializer
                                          (subseq cur-sentence
                                                  (+ startpos dimcount 1)))))))
                         (t (error "invalid function declaration form: ~a"
                                   cur-sentence))
                         ))
                      ))
                   (match-case (cur-sentence)
                     ('(?then-type c1-usercode::|if| (:paren . ?if-expr)
                        . ?then-part)
                       (let ((if-expr (c1-reparse-as-expr ?if-expr))
                             (then-part
                              (ecase ?then-type
                                (:statement
                                 (setf (elt paragraph idx)
                                       (cons ?then-type ?then-part))
                                 (transform-sentence idx)
                                 (elt paragraph idx))
                                (:form
                                 (assert (= (length ?then-part) 1))
                                 (c1-transform (cdar ?then-part)))))
                             (else-part
                              (when (< (1+ idx) (length paragraph))
                                (match-case ((elt paragraph (1+ idx)))
                                  ('(?else-type c1-usercode::|else| . ?else-part)
                                    (prog1
                                        (ecase ?else-type
                                          (:statement
                                           (setf (elt paragraph (1+ idx))
                                                 (cons ?else-type ?else-part))
                                           (transform-sentence (1+ idx))
                                           (elt paragraph (1+ idx)))
                                          (:form
                                           (assert (= (length ?else-part) 1))
                                           (c1-transform (cdar ?else-part))))
                                      (setf (cdr (nthcdr idx paragraph))
                                            (nthcdr (1+ (1+ idx)) paragraph))))
                                  (t nil)))))
                         (setf (elt paragraph idx)
                               `(if ,if-expr ,then-part
                                    ,@(when else-part (list else-part))))))
                     ('(?else-type c1-usercode::|else| . ?else-part)
                       (error "unmatched ELSE clause: ~a." ?else-part))
                     ('(?body-type c1-usercode::|while| (:paren . ?while-expr)
                        . ?body-part)
                       (let ((while-expr (c1-reparse-as-expr ?while-expr))
                             (body-part  (ecase ?body-type
                                           (:statement
                                            (setf (elt paragraph idx)
                                                  (cons ?body-type ?body-part))
                                            (transform-sentence idx)
                                            (elt paragraph idx))
                                           (:form
                                            (assert (= (length ?body-part) 1))
                                            (c1-transform (cdar ?body-part))))))
                         (setf (elt paragraph idx)
                               `(while ,while-expr
                                  ,@(when body-part (list body-part))))))
                     ('(:statement c1-usercode::|return| . ?return-value)
                       (setf (elt paragraph idx)
                             `(return
                                ,@(when ?return-value
                                        `(,(c1-reparse-as-expr ?return-value))))))
                     ('(:statement c1-usercode::|break|)
                       (setf (elt paragraph idx)
                             `(break)))
                     ('(:statement c1-usercode::|continue|)
                       (setf (elt paragraph idx)
                             `(loop)))
                     ('(:statement c1-usercode::|typedef| ?old-type-name . ?new-type-names)
                       (let ((new-types (split-sequence :comma ?new-type-names)))
                         (loop for new-type in new-types
                            do (push (first new-type) *c1-type-keyword-list*))
                         (setf (elt paragraph idx)
                               `(deftype (,?old-type-name) ,new-types))
                         ))
                     ('(:statement . ?arbitrary-expr)
                       (setf (elt paragraph idx)
                             (when ?arbitrary-expr
                               (c1-reparse-as-expr ?arbitrary-expr))))
                     ('(:brace . ?arbitrary-statements)
                       (setf (elt paragraph idx)
                             (c1-transform ?arbitrary-statements)))
                     (t
                      (error "unrecognized code segment: ~a" cur-sentence)))))))
    (let ((*c1-type-keyword-list* *c1-type-keyword-list*))
      (loop for i from 0
         if (>= i (length paragraph)) do (loop-finish)
         do (transform-sentence i)
         finally (return `(progn ,@paragraph))))))

(defun c1-add-main-call (code)
  (assert (and (consp code)
               (eq (car code) 'progn)))
  (append1 code '(APPLY C1-USERCODE::|main| NIL)))

(defun c1-parse (text)
  (let* ((lexer (c1-peekable-lexer text)))
    (c1-add-main-call (c1-transform (c1-paragraph lexer)))))

(defun c1-parse-expr (text)
  (let* ((lexer (c1-lexer text)))
    (parse-with-lexer lexer c1-expr-parser)))

(defun debug-print (&rest args)
  (apply 'format t args))

(defvar *c1-break-jmppoint-collector* :unset)
(defvar *c1-continue-jmppoint-collector* :unset)
(defvar *c1-return-info* :unset)

(defun c1-eir-gencode (ast)
  (let ((instructions nil)
        (strings nil))
    (labels ((.ip  ()
               (length instructions))
             (.ins (.f .l .v .t)
               (setf instructions (nconc instructions (list (list .f .l .v .t)))))
             (.str (str)
               (prog1 (length strings)
                 (setf strings (nconc strings (list str)))))
             (.fixaddr (.i .a)
               (setf (third (elt instructions .i)) .a))
             (.mins (.f .l .v .t) ; macro insructions
               (ecase .f
                 (:tofloat (.ins :lit 0 0.0 :float)
                           (.ins :opr 0 :add :opc))
                 (:tobool  (.ins :opr 0 :notnot :opc)
                           (.ins :opr 0 :notnot :opc))
                 (:toint   (.ins :lod 0 0 0)  ;ugly hack. 
                           (.ins :inc 0 -1 0) ;we'll use frame header (of type :int)
                           (.ins :sto 0 0 0)  ;to finish the conversion
                           (.ins :lod 0 0 0)
                           (.ins :inc 0 1 0)
                           (.ins :sto 0 0 0))
                 (:alloc   (ecase .l
                             (:int (.ins :lit 0 0 :int))
                             (:float (.ins :lit 0 0.0 :float))
                             (:bool  (.ins :lit 0 :false :bool))))
                 (:stokeep (.mins :sto .l .v .t)
                           (.ins :inc 0 1 :int))
                 (:sarkeep (.mins :sar .l .v .t)
                           (.ins :inc 0 1 :int))
                 ((:sto :lod :sar :lar)
                  (if (eql .l -1)
                      (.ins .f .l (1+ .v) .t)
                      (.ins .f .l .v .t)))
                 ))
             (.lev (scope &optional (curlevel 0))
               (if (second scope)
                   (.lev (second scope) (1+ curlevel))
                   curlevel))
             (.newscope (&optional parent)
               (list nil parent 4 (when parent (fourth parent))))
             (.findinscope (sym scope &optional (l 0))
               (let ((v (position sym (first scope) :key 'car)))
                 (cond
                   (v (values l v))
                   ((second scope) (.findinscope sym (second scope) (1+ l)))
                   (t nil))))
             (.newscopeitem (scope name type properties)
               (push (list name type properties) (first scope)))
             (.scopeitem  (lev idx scope)
               (dotimes (x lev) (setf scope (second scope)))
               (elt (first scope) idx))
             (.dup-scope  (scope)
               (copy-list scope))
             (.lowleveltype (type scope)
               (let ((type-atom (cond ((eq (first type) 'c1-usercode::|const|) (second type))
                                      (t (first type)))))
                 (case type-atom
                   ((c1-usercode::|float|) :float)
                   ((c1-usercode::|int|) :int)
                   ((c1-usercode::|bool|) :bool)
                   ((c1-usercode::|void|) :void)
                   (t (progn
                        (let ((aliased-type (cdr (assoc type-atom (fourth scope)))))
                          (if aliased-type
                              (.analyze-alloc aliased-type scope)
                              (error "no definition for type ~a" type-atom)))))
                   )))
             (.analyze-alloc (type scope)
               (let ((type-atom (cond ((eq (first type) 'c1-usercode::|const|) (second type))
                                      (t (first type)))))
                 (case type-atom
                   (c1-usercode::|int| (values :int 1))
                   (c1-usercode::|bool| (values :bool 1))
                   (c1-usercode::|float| (values :float 1))
                   (array (values (.analyze-alloc (second type) scope) (apply '* (third type))))
                   (t (progn
                        (let ((aliased-type (cdr (assoc type-atom (fourth scope)))))
                          (if aliased-type
                              (.analyze-alloc aliased-type scope)
                              (error "no definition for type ~a" type-atom)))))
                   )))
             (.gen (ast scope)
               (if (atom ast)
                   (cond
                     ((floatp ast)   (.ins :lit 0 ast :float) (values :float))
                     ((integerp ast) (.ins :lit 0 ast :int  ) (values :int))
                     ((symbolp  ast)
                      (case ast
                        ((nil) (values :void))
                        ((c1-usercode::|true|)
                         (.ins :lit 0 :true :bool)  (values :bool))
                        ((c1-usercode::|false|)
                         (.ins :lit 0 :false :bool) (values :bool))
                        (t
                         (multiple-value-bind (lev idx) (.findinscope ast scope)
                           (unless lev
                             (error "undeclared variable: ~a" ast))
                           (destructuring-bind (sym type properties)
                               (.scopeitem lev idx scope )
                             (declare (ignorable sym))
                                        ; (debug-print "DBG: ~a ~a~%" lev (.lev scope))
                                        ; (debug-print "DBG: ~a~%" scope)
                             (.mins :lod (if (eql lev (.lev scope)) -1 lev)
                                   (getf properties :addr)
                                   (.lowleveltype type scope))
                             (values (.lowleveltype type scope))))))))
                   (ecase (car ast)
                     ((+ - * / mod = /= > >= < <=)
                      (prog1
                          (eir::eir-arithmetic-promote
                           (.gen (second ast) scope)
                           (.gen (third  ast) scope))
                        (.ins :opr 0
                              (second
                               (assoc
                                (car ast)
                                '((+ :add) (- :minuss)
                                  (* :mult) (/ :divv) (mod :mod)
                                  (= :eq)  (/= :neq)
                                  (< :lt) (> :gt)
                                  (<= :lte) (>= :gte)))) :opc)))
                     (elt ;;TODO
                      (multiple-value-bind (arr idxlist)
                          (loop for form = ast then (second ast)
                             while (and (consp form) (eql (first form) 'elt))
                             collecting (third form) into ridxlist
                             finally (return (values form (reverse ridxlist))))
                        (assert (symbolp arr) () "invalid array variable: ~a" arr)
                        (multiple-value-bind (lev idx) (.findinscope arr scope)
                          (unless lev
                            (error "undeclared variable: ~a" arr))
                          (destructuring-bind (sym type properties)
                              (.scopeitem lev idx scope)
                            (declare (ignorable sym))
                            (assert (and (eq (first type) 'array)
                                         (consp (third type))
                                         (eql (length (third type))
                                              (length idxlist)))
                                    () "invalid array dimension used.")
                            (loop for i from 1 to (1- (length idxlist))
                               do(.gen (elt idxlist i) scope)
                                 (.gen (elt (third type) i) scope)
                                 (.ins :opr 0 :mult :opc))
                            (.gen (first (last idxlist)) scope)
                            (loop for i from 1 to (1- (length idxlist))
                               do(.ins :opr 0 :add :opc))
                            (.gen (apply '* (third type)) scope)
                            (.mins :lar (if (eql lev (.lev scope)) -1 lev)
                                  (getf properties :addr)
                                  (.lowleveltype (second type) scope))
                            (values (.lowleveltype (second type) scope))))))
                     ((and or)
                      (.gen (second ast) scope)
                      (let ((and (eq (car ast) 'and))
                            (cx1 (.ip)))
                        (.ins (if and :jpc :jpe) 0 0 0)
                        (.ins :lit 0 (if and :true :false) :bool)
                        (.gen (third ast) scope)
                        (.ins :opr 0 (if and :andand :oror) :opc)
                        (let ((cx2 (.ip)))
                          (.ins :jmp 0 0 :int)
                          (.fixaddr cx1 (.ip))
                          (.ins :lit 0 (if and :false :true) :bool)
                          (.fixaddr cx2 (.ip))))
                      (values :bool))
                     ((prog2)
                      (.gen (second ast) scope)
                      (.ins :inc 0 -1 0)
                      (.gen (third ast) scope))
                     ((coerce)
                      (.gen (third ast) scope)
                      (ecase (second ast)
                        (c1-usercode::|int|   (.mins :toint 0 0 0) (values :int))
                        (c1-usercode::|float| (.mins :tofloat 0 0 0)(values :float))
                        (c1-usercode::|bool|  (.mins :tobool 0 0 0)(values :bool))))
                     ((if)
                      (.gen (second ast) scope)
                      (let ((cx1 (.ip)))
                        (.ins :jpc 0 0 0)
                        (unless (eq (.gen (third ast) scope) :void)
                          (.ins :lit 0 -1 0))
                        (.fixaddr cx1 (.ip))
                        (when (fourth ast)
                          (let ((cx2 (.ip)))
                            (.ins :jmp 0 0 0)
                            (.fixaddr cx1 (.ip))
                            (unless (eq (.gen (fourth ast) scope) :void)
                              (.ins :lit 0 -1 0))
                            (.fixaddr cx2 (.ip)))))
                      (values :void))
                     ((while)
                      (let ((cx1 (.ip)))
                        (.gen (second ast) scope)
                        (let ((cx2 (.ip)))
                          (let ((*c1-break-jmppoint-collector* nil)
                                (*c1-continue-jmppoint-collector* nil))
                            (.ins :jpc 0 0 0)
                            (unless (eq (.gen (third ast) scope) :void)
                              (.ins :lit 0 -1 0))
                            (.ins :jmp 0 cx1 0)
                            (loop for jmppoint in *c1-break-jmppoint-collector*
                               do (.fixaddr jmppoint (.ip)))
                            (loop for jmppoint in *c1-continue-jmppoint-collector*
                               do (.fixaddr jmppoint cx1))
                            )
                          (.fixaddr cx2 (.ip))))
                      (values :void))
                     ((break)
                      (assert (not (eq *c1-break-jmppoint-collector* :unset))
                              () "invalid position for break statement.")
                      (push (.ip) *c1-break-jmppoint-collector* )
                      (.ins :jmp 0 0 :int)
                      (values :void)
                      )
                     ((loop)
                      (assert (not (eq *c1-continue-jmppoint-collector* :unset))
                              () "invalid position for continue statement.")
                      (push (.ip) *c1-continue-jmppoint-collector* )
                      (.ins :jmp 0 0 :int)
                      (values :void)
                      )
                     ((progn)
                      (let ((scope (.dup-scope scope)))
                        (if (rest ast)
                            (progn 
                              (loop for item in #|(butlast |#(rest ast)#|)|#
                                 do (unless (eql (.gen item scope) :void)
                                      (.ins :inc 0 -1 0)))
                              #|(.gen (car (last ast)) scope))|#
                              (values :void))
                            (values :void))))
                     ((declare)
                      (loop for declr in (rest ast)
                         do (destructuring-bind
                                  (declr-name declr-type &optional declr-value) declr
                              (cond
                                ((eq (first declr-type) 'function)
                                 (progn
                                   (let ((cx1 (.ip)))
                                     (.ins :jmp 0 0 :int)
                                     (let ((cx2 (.ip)))
                                       (.ins :jmp 0 0 :int)
                                       (.fixaddr cx1 (.ip))
                                       (.newscopeitem scope declr-name declr-type
                                                      `(:type :function
                                                              :addr ,cx2 :stub t))))))
                                (t
                                 (progn
                                   (let ((cx1 (.ip))
                                         (dx1 (third scope)))
                                     (multiple-value-bind (alloc-type alloc-count)
                                         (.analyze-alloc declr-type scope)
                                       (loop for i from 1 to alloc-count
                                          do (.mins :alloc alloc-type 0 0)
                                            (incf (third scope)))
                                       (when declr-value
                                         (if (eq (first declr-type) 'array)
                                             (labels ((flattenize-initializer (dims values)
                                                        (if (= (length dims) 1)
                                                            (loop for i from 1 to (first dims)
                                                               collecting (nth (1- i) values))
                                                            (loop for i from 1 to (first dims)
                                                               appending (flattenize-initializer (rest dims) (nth (1- i) values))))))
                                               (let* ((flat-initializer (flattenize-initializer (third declr-type) declr-value)))
                                                 (loop for i from 0
                                                    for item in flat-initializer
                                                    do (when item
                                                         (.gen item scope)
                                                         (.ins :sto 0 (+ dx1 i) :int)))))
                                             (progn
                                               (.gen declr-value scope)
                                               (.ins :sto 0 dx1 :int)))
                                         )
                                       )
                                     (.newscopeitem scope declr-name declr-type
                                                    `(:type :variable
                                                            :addr ,dx1)))))
                                )))
                      (values :void))
                     ((defun)
                      (let ((cx1 (.ip)))
                        (.ins :jmp 0 0 :int)
                        (let ((cx2 (.ip)))
                          (multiple-value-bind (lev idx)
                              (.findinscope (second ast) scope)
                            (if lev
                                (destructuring-bind (sym type properties)
                                    (.scopeitem lev idx scope)
                                  (if (eq (getf properties :stub) t)
                                      (progn
                                        (.fixaddr (getf properties :addr) cx2)
                                        (setf (getf properties :stub) nil))
                                      (progn
                                        (error "multiple definitions of function ~a"
                                               (second ast)))))
                                (progn
                                  (.newscopeitem scope (second ast)
                                                 `(function ,(fourth ast)
                                                            ,(mapcar 'car (third ast)))
                                                 `(:type :function
                                                         :addr ,cx2 :stub nil)))))
                          (let ((newscope (.newscope scope)))
                            (.ins :inc 0 3 :int)
                            (let ((rettype (.lowleveltype (fourth ast) scope))
                                  (funargcount (length (third ast))))
                              (.mins :alloc (if (eq rettype :void) :int rettype) 0 0)
                              (let ((*c1-return-info* (list rettype funargcount)))
                                (loop
                                   for addr downfrom -1
                                   for arg in (reverse (third ast))
                                   do (when (> (length arg) 1)
                                        (.newscopeitem newscope (second arg)
                                                       (first arg)
                                                       `(:type :variable
                                                               :addr ,addr))
                                        (.ins :init 0 addr (.lowleveltype (first arg) scope))
                                        ))
                                (.gen (fifth ast) newscope)
                                (.ins :opr funargcount :ret :opc)
                                )))
                          )
                        (.fixaddr cx1 (.ip))
                        )
                      (values :void)
                      )
                     ((setf)
                      (prog1
                          (.gen (third ast) scope)
                        (if (atom (second ast))
                            (progn
                              (multiple-value-bind (lev idx)
                                  (.findinscope (second ast) scope)
                                (unless lev
                                  (error "undeclared variable: ~a" (second ast)))
                                (destructuring-bind (sym type properties)
                                    (.scopeitem lev idx scope )
                                  (let ((storage-count (nth-value 1 (.analyze-alloc type scope))))
                                    (if (eql storage-count 1)
                                        (progn (.mins :stokeep (if (eql lev (.lev scope)) -1 lev)
                                                      (getf properties :addr)
                                                      :int)
                                               (.lowleveltype type scope))
                                        (progn
                                          (loop for i below storage-count
                                             do (.mins :sto (if (eql lev (.lev scope)) -1 lev)
                                                       (+ i (getf properties :addr))
                                                       :int))
                                          (values :void)))))))
                            (progn
                              (assert (and (consp (second ast))
                                           (eq    (first (second ast)) 'elt))
                                      () "lvalue must be supplied: ~a" (second ast))
                              (multiple-value-bind (arr idxlist)
                                  (loop for form = (second ast) then (second form)
                                     while (and (consp form) (eql (first form) 'elt))
                                     collecting (third form) into ridxlist
                                     finally (return (values form (reverse ridxlist))))
                                (assert (symbolp arr) () "invalid array variable: ~a" arr)
                                (multiple-value-bind (lev idx) (.findinscope arr scope)
                                  (unless lev
                                    (error "undeclared variable: ~a" arr))
                                  (destructuring-bind (sym type properties)
                                      (.scopeitem lev idx scope)
                                    (declare (ignorable sym))
                                    (assert (and (eq (first type) 'array)
                                                 (consp (third type))
                                                 (eql (length (third type))
                                                      (length idxlist)))
                                            () "invalid array dimension used.")
                                    (loop for i from 1 to (1- (length idxlist))
                                       do(.gen (elt idxlist i) scope)
                                         (.gen (elt (third type) i) scope)
                                         (.ins :opr 0 :mult :opc))
                                    (.gen (first (last idxlist)) scope)
                                    (loop for i from 1 to (1- (length idxlist))
                                       do(.ins :opr 0 :add :opc))
                                    
                                    (.gen (apply '* (third type)) scope)
                                    (.mins :sar (if (eql lev (.lev scope)) -1 lev)
                                          (getf properties :addr)
                                          (.lowleveltype (second type) scope))
                                    (values :void)))))
                            )))
                   ((apply)
                    (cond
                      ((eq (second ast) 'c1-usercode::|write|)
                       (loop for arg in (third ast)
                          do (if (stringp arg)
                                 (progn
                                   (.ins :lit 0 (.str arg) :int)
                                   (.ins :opr 0 :writes :opc))
                                 (progn
                                   (.gen arg scope)
                                   (.ins :opr 0 :writee :opc))))
                       (values :void))
                      ((eq (second ast) 'c1-usercode::|read|)
                       (loop for arg in (third ast)
                          do(if (atom arg)
                                (progn
                                  (multiple-value-bind (lev idx)
                                      (.findinscope arg scope)
                                    (unless lev
                                      (error "undeclared variable: ~a" arg))
                                    (destructuring-bind (sym type properties)
                                        (.scopeitem lev idx scope )
                                      (.ins :opr (.lowleveltype type scope) :readd :opc)
                                      (.mins :sto (if (eql lev (.lev scope)) -1 lev)
                                            (getf properties :addr)
                                            :int))))
                                (progn
                                  (assert (and (consp arg)
                                               (eq    (first arg) 'elt))
                                          () "lvalue must be supplied: ~a" arg)
                                  (multiple-value-bind (arr idxlist)
                                      (loop for form = arg then (second form)
                                         while (and (consp form) (eql (first form) 'elt))
                                         collecting (third form) into ridxlist
                                         finally (return (values form (reverse ridxlist))))
                                    (assert (symbolp arr) () "invalid array variable: ~a" arr)
                                    (multiple-value-bind (lev idx) (.findinscope arr scope)
                                      (unless lev
                                        (error "undeclared variable: ~a" arr))
                                      (destructuring-bind (sym type properties)
                                          (.scopeitem lev idx scope)
                                        (declare (ignorable sym))
                                        (assert (and (eq (first type) 'array)
                                                     (consp (third type))
                                                     (eql (length (third type))
                                                          (length idxlist)))
                                                () "invalid array dimension used.")
                                        (.ins :opr (.lowleveltype (second type) scope)
                                              :readd :opc)
                                        (loop for i from 1 to (1- (length idxlist))
                                           do(.gen (elt idxlist i) scope)
                                             (.gen (elt (third type) i) scope)
                                             (.ins :opr 0 :mult :opc))
                                        (.gen (first (last idxlist)) scope)
                                        (.gen (apply '* (third type)) scope)
                                        (.mins :sar (if (eql lev (.lev scope)) -1 lev)
                                              (getf properties :addr)
                                              (.lowleveltype (second type) scope))
                                        (values :void)))))
                                ))
                       (values :void))
                    (t
                     (loop for arg in (third ast)
                        do (.gen arg scope))
                     (multiple-value-bind (lev idx)
                         (.findinscope (second ast) scope)
                       (unless lev
                         (error "undeclared function: ~a" (second ast)))
                       (destructuring-bind (sym type properties)
                           (.scopeitem lev idx scope)
                         (when (/= (length (third ast)) (length (third type)))
                           (error "funcall argument count mismatch: ~a"
                                  (second ast)))
                         (.ins :cal 0 (getf properties :addr) :int)
                         (let ((rettype (.lowleveltype (second type) scope)))
                           (when (eq rettype :void)
                             (.ins :inc 0 -1 :int))
                           rettype))))))
                   ((return)
                    (assert (not (eql *c1-return-info* :unset)))
                    (if (eq (first *c1-return-info*) :void)
                        (progn
                          (assert (= (length ast) 1)
                                  () "return statement invalid: ~a" ast))
                        (progn
                          (assert (= (length ast) 2)
                                  () "return statement invalid: ~a" ast)
                          (.gen (second ast) scope)
                          (.ins :sto 0 3 :int)
                          ))
                    (.ins :opr (second *c1-return-info*) :ret :opc)
                    (values :void)
                    )
                   ((deftype)
                    (loop for new-type in (third ast)
                       do (push (cons (first new-type) (second ast)) (fourth scope)))
                    (values :void)
                    )
                   ))))
      (.ins :inc 0 2 :int)
      (.ins :lit 0 0 :int)
      (.ins :inc 0 1 :int)
      (.gen ast (.newscope))
      (.ins :opr 0 :ret :opc)
      (.fixaddr 1 (length instructions)) ;keep compability with old p-code
      (list instructions strings))))
