(cl:defpackage #:c1cc
  (:use #:cl #:c1))

(cl:in-package #:c1cc)

(defun read-file (&optional (stream *standard-input*))
  (with-output-to-string (s)
    (loop
       do (let ((l (read-line stream nil nil)))
            (unless l
              (loop-finish))
            (write-line l s)))))

(defvar *quit-on-error* t)

(defun c1cc-fake-debugger (condition foo)
  (declare (ignore foo))
  (format *error-output* "~&Error: ~A" condition)
  (when *quit-on-error*
    (let ((c (find-restart 'continue)))
      (when c
        (invoke-restart c))
      #+ccl (ccl:quit)
      #+sbcl (sb-ext:quit)
      #+ecl  (ext:quit))))
  

(defun main()
  (eval-when (:load-toplevel :execute)
    (let ((*debugger-hook* #'c1cc-fake-debugger))
      (eir::write-asm
       (c1::c1-eir-gencode
        (c1::c1-parse (read-file)))))))

(defun fmain(filename)
  (with-open-file (*standard-input* filename :direction :input)
    (main)))

(defun fparse(filename)
  (with-open-file (*standard-input* filename :direction :input)
    (print (c1::c1-parse (read-file)))))
