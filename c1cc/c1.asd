;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-

(cl:in-package #:asdf)

(defsystem #:c1
  :version "0.0.1"
  :description "A C-like language compiler"
  :author "Charles Lew <crlf0710@gmail.com>"
  :depends-on(#+ecl "prebuilt-asdf"
              "cl-lex" "yacc"
              "alexandria" "cl-unification" "split-sequence")
  :serial t
  :components
  ((:file "eir")
   (:file "c1")
   (:file "c1cc")
   ))
